# Changelog

## 0.1.3

  * Document which Redux lib it works with

## 0.1.2

  * Add license to satisfy Pub.

## 0.1.1

  * Fix dependencies


## 0.1.0

  * Initial version, includes a `ReduxDevTools` Widget that allows you to time travel through the states of your `DevToolsStore`. Requires Dart redux and redux_dev_tools.
